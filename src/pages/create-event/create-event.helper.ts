export module CreateEventHelper {
    export function isMultiSwitchPin(data) {
        return !!data.options;
    }

    export function getInviteCode() {
        var code = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (var i = 0; i < 5; i++)
            code += possible.charAt(Math.floor(Math.random() * possible.length));

        return code;
    }
}