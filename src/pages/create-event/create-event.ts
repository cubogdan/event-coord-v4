import { Component } from '@angular/core';
import { AlertController, ModalController, NavController, ToastController } from 'ionic-angular';

import { mapCoordinates } from "../../models/coordinates.map";
import { Place } from "../../models/place.map";

import { EventProvider } from "../../providers/event/event";

import * as Pins from "../../models/pin.types"
import { CreateEventHelper } from "./create-event.helper";

@Component({
    selector   : 'page-create-event',
    templateUrl: 'create-event.html'
})
export class CreateEventPage {
    pinTypesArray           = Pins.getTypesList();
    selectedPinType: string = 'counter';

    eventName: string;
    mapPins: Place[] = [];

    centralEventLocation: mapCoordinates = {
        lat: 45.747078,
        lng: 21.231615
    };

    constructor(private navCtrl: NavController,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private modalCtrl: ModalController,
                private eventProvider: EventProvider) {
    }

    ionViewDidLoad() {
        this._getEventNameFromUser();
    }

    addMarker(event: any) {
        if (!this.selectedPinType) {
            this.toastCtrl.create({
                message        : 'Please select a pin type first!',
                duration       : 3000,
                position       : 'top',
                showCloseButton: true,
                closeButtonText: 'OK'
            }).present();
            return;
        }

        let myModal = this.modalCtrl.create(Pins.getPinComponent(this.selectedPinType));
        myModal.onDidDismiss(data => this._saveMarker(event.coords, data));
        myModal.present();
    }

    private _saveMarker(coords, data) {
        // TODO: Check this funciton
        console.log('inside saveMarker(), data: ', data);
        let marker;

        if (CreateEventHelper.isMultiSwitchPin(data)) {
            marker = new Place(
                data.title,
                new mapCoordinates(coords.lat, coords.lng),
                this.selectedPinType,
                data.options
            );
        } else {
            marker = new Place(
                data.title,
                new mapCoordinates(coords.lat, coords.lng),
                this.selectedPinType
            );
        }

        this.mapPins.push(marker);
    }

    saveEvent() {
        console.log('here');
        let eventData = {
            name: this.eventName,
            code: CreateEventHelper.getInviteCode(),
            pins: this.mapPins
        };

        console.log(eventData);

        this.eventProvider.saveEvent(eventData);

        let alert = this.alertCtrl.create({
            title: 'Invitation code:',
            subTitle: eventData.code,
            buttons: ['OK']
        });
        alert.present();
        this.navCtrl.pop();
    }

    selectPinType(type) {
        this.selectedPinType = type;
    }

    private _getEventNameFromUser() {
        let alert = this.alertCtrl.create({
            title  : 'Enter event name',
            inputs : [
                {
                    name       : 'name',
                    placeholder: 'Enter your event name here'
                }
            ],
            buttons: [
                {
                    text   : 'Cancel',
                    role   : 'cancel',
                    handler: () => {
                        this.navCtrl.pop();
                    }
                },
                {
                    text   : 'Continue',
                    handler: data => data.name
                }
            ]
        });
        alert.onDidDismiss(data => this.eventName = data.name);
        alert.present();
    }
}


