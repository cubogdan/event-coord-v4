import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreateEventPage } from "../create-event/create-event";
import { EventPage } from "../event/event";
import { EventProvider } from "../../providers/event/event";

@IonicPage()
@Component({
    selector   : 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    createEventPage = CreateEventPage;

    invitationCode: string = 'aaa';

    constructor(private navCtrl: NavController,
                private alertCtrl: AlertController,
                private eventProvider: EventProvider) {
    }

    ionViewDidLoad() {
        // this.joinEvent();
    }

    async joinEvent() {
        const eventData = await this.eventProvider.doesEventExist(this.invitationCode);
        if (!eventData) {
            this.alertCtrl.create({
                title   : 'Event not found',
                subTitle: 'The code you entered is invalid',
                buttons : ['OK']
            }).present();
            return;
        }

        for (let key in eventData) {
            if (!eventData.hasOwnProperty(key)) return;
            this.navCtrl.push(EventPage, { eventID: key });
            return;
        }

        // TODO: Find another option for storing the event data locally, nativeStorage is buggy on Ionic, use ionicStorage maybe?

        // this.nativeStorage.setItem('event', {
        //     _uid: 'eventData',
        //     anotherProperty: 'anotherValue'
        // })
        //     .then(
        //         () => console.log('Stored item!'),
        //         error => console.error('Error storing item', error)
        //     );
    }

}
