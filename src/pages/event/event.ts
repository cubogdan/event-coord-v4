import { Component } from '@angular/core';
import {
    AlertController,
    ModalController,
    NavController,
    NavParams,
    ToastController,
    ActionSheetController
} from 'ionic-angular';

import { mapCoordinates } from "../../models/coordinates.map";
import { Place } from "../../models/place.map";

import { EventProvider } from "../../providers/event/event";

import * as Pins from "../../models/pin.types"
import { EventHelper } from "./event.helper";
import { Observable } from "rxjs/Observable";

@Component({
    selector   : 'page-event',
    templateUrl: 'event.html'
})
export class EventPage {
    pinTypesArray = Pins.getTypesList();

    eventName: string;
    mapPins: Place[] = [];

    eventID: string;
    eventDataObservable: Observable<any>;

    centralEventLocation: mapCoordinates = {
        lat: 45.747078,
        lng: 21.231615
    };

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private alertCtrl: AlertController,
                private actionSheetCtrl: ActionSheetController,
                private toastCtrl: ToastController,
                private modalCtrl: ModalController,
                private eventProvider: EventProvider) {
        // let eventData
        // this.eventName =
        this.eventID             = this.navParams.get('eventID');
        this.eventDataObservable = eventProvider.getEventObservable(this.eventID);

        this.eventDataObservable.subscribe(eventData => {
            this.mapPins = eventData.pins;
        });
    }

    ionViewDidLoad() {

    }

    onPinClick(index) {
        console.log(`pin index: `, index);
        console.log(this.mapPins[index]);

        if (this.mapPins[index].type === 'switch') {
            this._toggleSwitchPin(index, this.mapPins[index]['state']);
        } else if (this.mapPins[index].type === 'counter') {
            this._openCounterPinController(this.mapPins[index], index);
        } else {
            this._openMultiSwitchPinController(this.mapPins[index], index);
        }
    }

    private _toggleSwitchPin (index, state) {
        if (state === 'ON') {
            this.eventProvider.changePinState(this.eventID, index, 'OFF')
        } else {
            this.eventProvider.changePinState(this.eventID, index, 'ON')
        }
    }

    private _openCounterPinController(pinData, index) {
        const actionSheet = this.actionSheetCtrl.create({
            title  : `State: ${ pinData.state || 0 }`,
            buttons: [
                {
                    text   : 'Increment',
                    handler: () => {
                        if (pinData.state === 0) {
                            pinData.state = 1;
                        } else {
                            pinData.state++;
                        }

                        this.eventProvider.changePinState(this.eventID, index, pinData.state);
                        actionSheet.setTitle(`State: ${ pinData.state || 0 }`);
                        return false;
                    }
                },{
                    text   : 'Decrement',
                    handler: () => {
                        if (pinData.state === 0) {
                            pinData.state = -1;
                        } else {
                            pinData.state--;
                        }
                        this.eventProvider.changePinState(this.eventID, index, pinData.state);
                        actionSheet.setTitle(`State: ${ pinData.state || 0 }`);
                        return false;
                    }
                }, {
                    text   : 'Back',
                    role   : 'cancel'
                }
            ]
        });
        actionSheet.present();
    }

    private _openMultiSwitchPinController(pinData, pinIndex) {
        let buttons = [];
        const alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        pinData.data.forEach((option, index) => {
            buttons.push({
                text   : `${ alphabet[index] }: ${ option }`,
                handler: () => {
                    this.eventProvider.changePinState(this.eventID, pinIndex, alphabet[index]);
                    actionSheet.setTitle(`State: ${ option }`);
                }
            })
        });

        buttons.push({
            text   : 'Back',
            role   : 'cancel'
        });

        const actionSheet = this.actionSheetCtrl.create({
            title  : `State: ${ pinData.state || 'NONE' }`,
            buttons
        });
        actionSheet.present();
    }

    getFormatedPinState(state, type, index) {
        if (type === 'counter'){
            if (!state) return "0";
            return state.toString();
        } else if (type === 'multiSwitch') {
            if (!state) return true;

            return state;
            // const alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            // return alphabet[index];
        } else {
            return state;
        }
        // if (state !== undefined && state !== null) return state.toString();
        // return true;
    }
}


