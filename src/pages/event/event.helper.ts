export module EventHelper {
    export function isMultiSwitchPin(data) {
        return !!data.options;
    }
}