import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { CreateEventPage } from '../pages/create-event/create-event';
import { EventPage } from "../pages/event/event";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AgmCoreModule } from '@agm/core'

import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";

import { environment } from "../environments/environments";

import { EventProvider } from '../providers/event/event';
import { CounterComponent } from "../shared/pins/counter/counter.component";
import { SwitchComponent } from "../shared/pins/switch/switch.component";
import { MultiSwitchComponent } from "../shared/pins/multi-switch/multi-switch.component";
import { HomePage } from "../pages/home/home";
import { NativeStorage } from "@ionic-native/native-storage";

@NgModule({
    declarations   : [
        MyApp,
        CreateEventPage,
        EventPage,
        HomePage,
        CounterComponent,
        SwitchComponent,
        MultiSwitchComponent
    ],
    imports        : [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        AgmCoreModule.forRoot({
            apiKey: environment.agm.apiKey
        })
    ],
    bootstrap      : [IonicApp],
    entryComponents: [
        MyApp,
        CreateEventPage,
        EventPage,
        HomePage,
        CounterComponent,
        SwitchComponent,
        MultiSwitchComponent
    ],
    providers      : [
        StatusBar,
        NativeStorage,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        EventProvider
    ]
})
export class AppModule {
}
