import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    templateUrl: 'switch.html'
})
export class SwitchComponent {
    title: string;

    constructor(private viewCtrl: ViewController) {

    }

    dismiss() {
        this.viewCtrl.dismiss({ title: this.title });
    }
}