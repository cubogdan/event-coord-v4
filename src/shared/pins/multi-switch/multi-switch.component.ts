import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    templateUrl: 'multi-switch.html'
})
export class MultiSwitchComponent {
    title: string;
    options: string[] = ['', ''];

    constructor(private viewCtrl: ViewController) {

    }

    addOption() {
        this.options.push('')
    }

    savePin() {
        this.viewCtrl.dismiss({
            title: this.title,
            options: this.options
        });
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

    trackByIndex(index: number, value: number) {
        return index;
    }
}