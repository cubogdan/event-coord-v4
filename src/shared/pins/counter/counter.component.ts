import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    templateUrl: 'counter.html'
})
export class CounterComponent {
    title: string;

    constructor(private viewCtrl: ViewController) {

    }

    dismiss() {
        this.viewCtrl.dismiss({ title: this.title });
    }
}