import { mapCoordinates } from "./coordinates.map";

export class Place {
    constructor(public title: string,
                public location: mapCoordinates,
                public type: string,
                public data: object = {}) {
    }
}
