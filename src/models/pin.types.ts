import { CounterComponent } from "../shared/pins/counter/counter.component";
import { SwitchComponent } from "../shared/pins/switch/switch.component";
import { MultiSwitchComponent } from "../shared/pins/multi-switch/multi-switch.component";

const types: Object = {
    counter: {
        name: 'Counter',
        type: 'counter',
        icon: 'speedometer',
        component: CounterComponent
    },
    switch: {
        name: 'Switch',
        type: 'switch',
        icon: 'power',
        component: SwitchComponent
    },
    multiSwitch: {
        name: 'Multiple Switch',
        type: 'multiSwitch',
        icon: 'switch',
        component: MultiSwitchComponent
    }
};

// const typesArray: string[] = Object.keys(types);

function getTypesList() {
    return Object.keys(types);
}

function getPinComponent(pinType) {
    return types[pinType].component;
}

export {
    // types,
    getTypesList,
    getPinComponent
};

// export class PinTypes {
//     public data: object   = {
//         counter    : {
//             name     : 'Counter',
//             type     : 'counter',
//             icon     : '',
//             component: CounterComponent
//         },
//         switch     : {
//             name     : 'Switch',
//             type     : 'switch',
//             icon     : '',
//             component: SwitchComponent
//         },
//         multiSwitch: {
//             name     : 'Multiple Switch',
//             type     : 'multiSwitch',
//             icon     : '',
//             component: MultiSwitchComponent
//         }
//     };
//     public list: String[] = Object.keys(this.data);
//
//     constructor() {
//
//     }
// }
