import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";

@Injectable()
export class EventProvider {
    constructor(private db: AngularFireDatabase) {

    }

    public saveEvent(eventData) {
        return this.db.list('events').push(eventData)
    }

    public async doesEventExist(eventID) {
        try {
            let eventRef;

            await this.db.list('events', ref => {
                eventRef = ref;

                return ref;
            }).valueChanges();

            return eventRef
                .orderByChild('code')
                .equalTo(eventID)
                .once('value')
                .then(snap => snap.val());

        } catch (e) {
            console.log(e);
            return false;
        }
    }

    public getEventObservable(eventID) {
        return this.db.object(`events/${ eventID }`).valueChanges();
    }

    public changePinState(eventID, pinIndex, state) {
        const itemRef = this.db.object(`events/${ eventID }/pins/${ pinIndex }`);
        return itemRef.update({ state });
    }
}
